// the submit handler
$('#submit').click(() => {
  let challenge = $('#challenge').val();
  let flag = $('#flag').val();
  let mail = $('#email').val();
  let compiled_url = '/solve/' + challenge + '/' + flag + '/' + mail;
  console.log(compiled_url);
  $.ajax({
    url: compiled_url,
    success: (res) => {
      $('#result')
        .fadeIn('fast')
        .text(res)
        .delay(3000)
        .fadeOut('slow');
    },
    error: (res) => {
      console.log('Error!');
    }
  })
})
