import os
import pickle
import time

class Solver:
    def __init__(self, email):
        self.email = email
        self.stamp = 0
        self.solved = set()

def init_files():
    if os.path.isfile('solvers.dat'):
        pass
    else:
        with open('solvers.dat', 'wb') as f:
            pickle.dump(list(), f);

def getflag():
    urls = dict()
    flags = dict()
    with open('flags.txt') as f:
        for line in f:
            print(line.strip().split(' '))
            challenge, flag, url = line.strip().split(' ')
            flags[challenge] = flag
            urls[challenge] = url
    return flags, urls
    
def get_solvers():
    solvers = list()
    with open('solvers.dat', 'rb') as f:
        solvers = pickle.load(f);
    return solvers
    
def add_solver(challenge, email):
    with open('solvers.dat', 'rb') as f:
        solvers = pickle.load(f)
    
    new = True
            
    for solver in solvers:
        if solver.email == email:
            if not challenge in solver.solved:
                solver.solved.add(challenge)
                solver.stamp = time.time()
            new = False
            break
    
    if new:
        solver = Solver(email)
        solver.solved.add(challenge)
        solver.stamp = time.time()
        solvers.append(solver)

    with open('solvers.dat', 'wb') as f:
        pickle.dump(solvers, f)

