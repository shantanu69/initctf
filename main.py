from flask import Flask, render_template
from fileutils import getflag, add_solver, get_solvers, init_files

app = Flask(__name__)
init_files()

# The solutions
CHALLENGES, URLS = getflag()

@app.route('/')
def index():
    return render_template('index.html', challenges=URLS)

@app.route('/submit')
def submit():
    return render_template('submit.html', challenges=CHALLENGES.keys())

@app.route('/solve/<challenge>/<flag>/<email>')
def submission(challenge, flag, email):
    if flag == CHALLENGES[challenge]:
        add_solver(challenge, email)
        return 'Congrats!', 200, {'Content-Type': 'text/plain'}
    else:
        return 'Wrong!', 200, {'Content-Type': 'text/plain'}

def comparesolvers(s1, s2):
    if len(s1.solved) > len(s2.solved):
        return 1
    elif len(s2.solved) > len(s1.solved):
        return -1
    else:
        if s1.stamp < s2.stamp:
            return 1
        else:
            return -1

def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0  
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K

@app.route('/leaderboard')
def leaderboard():
    solvers = sorted(get_solvers(), key=cmp_to_key(comparesolvers), reverse=True)
    board = dict()
    print(solvers)
    for solver in solvers:
        board[solver.email] = []
        for challenge in CHALLENGES.keys():
            board[solver.email].append(challenge in solver.solved)

    return render_template('leaderboard.html', challenges=CHALLENGES.keys(), board=board)

@app.route('/getflag/58aab65a7f652a0f638a13272cbb902c')
def getflag():
    return 'flag{client_side_is_the_dark_side}', 200, {'Content-Type': 'text/plain'}


if __name__ == '__main__':
    app.run(debug=True)